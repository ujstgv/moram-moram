package com.lechat.lifestreaming;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lechat.lifestreaming.adapter.LCCalendarViewAdapter;
import com.lechat.lifestreaming.global.LCApplication;
import com.lechat.lifestreaming.interfaces.IPendingActionListener;
import com.lechat.lifestreaming.model.LCWeekOnCalendar;
import com.lechat.lifestreaming.utils.LCDateUtils;
import com.lechat.lifestreaming.utils.lo;

/**
 * Created by lechat on 16. 4. 19..
 */
public class CalendarActivity extends AppCompatActivity implements IPendingActionListener {

    private RecyclerView vCalendar;
    private LCCalendarViewAdapter mAdapter;

    private LCWeekOnCalendar[] calendarDataSet = null;
    private int focusedMonth;

    private void init() {
        setContentView(R.layout.activity_calendar);

        vCalendar = (RecyclerView) findViewById(R.id.view_calendar);
        vCalendar.setLayoutManager(new LinearLayoutManager(this));
        vCalendar.addOnScrollListener(lScrollRecyclerView);
    }

    private void initCalendarDataSet() {
        calendarDataSet = ((LCApplication) getApplication()).getCalendarDataSet(this);

        if (calendarDataSet != null) {
            mAdapter = new LCCalendarViewAdapter(calendarDataSet);
            vCalendar.setAdapter(mAdapter);

            moveToToday();
        }
    }

    public void doAction() {
        initCalendarDataSet();
    }

    private RecyclerView.OnScrollListener lScrollRecyclerView = new RecyclerView.OnScrollListener() {

        /**
         * Callback method to be invoked when RecyclerView's scroll state changes.
         *
         * @param recyclerView The RecyclerView whose scroll state has changed.
         * @param newState     The updated scroll state. One of
         *                     SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
         */
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                onChangeFocusedMonth(getListPositionIndex());
            }
        }

        /**
         * Callback method to be invoked when the RecyclerView has been scrolled. This will be
         * called after the scroll has completed.
         */
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//            mAdapter.setCurrentMonth(calendarDataSet[getListPositionIndex()].month2);
        }
    };

    private void onChangeFocusedMonth(int weekIndex) {

        lo.v("on change focused month");

        LCWeekOnCalendar _targetWeek = calendarDataSet[weekIndex];
        int _monthOfListTop = _targetWeek.month2;
        if (focusedMonth == _monthOfListTop) return;

        focusedMonth = _monthOfListTop;
        mAdapter.setCurrentMonth(focusedMonth);

        if (getSupportActionBar() != null) {
            String _newTitle = String.format("%d년 %d월", _targetWeek.year, _targetWeek.month2 + 1);
            getSupportActionBar().setTitle(_newTitle);
        }
    }

    private void moveToToday() {

        int _indexForToday = LCDateUtils.getWeekIndexOfToday(calendarDataSet);
        vCalendar.scrollToPosition(_indexForToday);
        onChangeFocusedMonth(_indexForToday);
    }

    private int getListPositionIndex() {

        lo.e("getListIndex...",
                ((LinearLayoutManager) vCalendar.getLayoutManager()).findFirstCompletelyVisibleItemPosition());

        return ((LinearLayoutManager) vCalendar.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
    }

    /*
    ================================================================================================
    Activity event
    ================================================================================================
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initCalendarDataSet();
    }
}
