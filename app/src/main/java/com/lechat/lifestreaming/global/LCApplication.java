package com.lechat.lifestreaming.global;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.lechat.lifestreaming.interfaces.IPendingActionListener;
import com.lechat.lifestreaming.model.LCWeekOnCalendar;
import com.lechat.lifestreaming.services.CalendarDataService;
import com.lechat.lifestreaming.utils.lo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lechat on 16. 4. 19..
 */

public class LCApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        lo.w("App > create");

//        bindServiceForCalendarData();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        lo.w("App > terminate");

//        unbindServiceForCalendarData();
    }

    /*
    ================================================================================================

    ================================================================================================
     */

    private LCWeekOnCalendar[] calendarDataSet = null;
    private CalendarDataService service = null;
    private boolean isOnConnect = false;
    private List<IPendingActionListener> pendingActionListeners = new ArrayList<>();

    public LCWeekOnCalendar[] getCalendarDataSet(IPendingActionListener pendingActionListener) {
        if (calendarDataSet == null) {
            pendingActionListeners.add(pendingActionListener);
            setCalendarDataSet();

            return null;
        }
        return calendarDataSet;
    }

    private void bindServiceForCalendarData() {
        if (isOnConnect) {
            return;
        }

        lo.e("bind service");

        isOnConnect = true;
        Intent _service = new Intent(this, CalendarDataService.class);
        bindService(_service, calendarDataConnection, Context.BIND_AUTO_CREATE);
        registerReceiver(rCalendarDataSet, new IntentFilter(LCBroadcast.CALENDAR_DATA_IS_READY));
    }

    private void unbindServiceForCalendarData() {

        lo.e("unbind service");

        isOnConnect = false;
        unbindService(calendarDataConnection);
        unregisterReceiver(rCalendarDataSet);
    }

    private final ServiceConnection calendarDataConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {

            lo.e("service connected...", name.getClassName(), binder);

            if (binder != null && binder instanceof CalendarDataService.LocalBinder) {
                isOnConnect = false;
                service = ((CalendarDataService.LocalBinder) binder).getService();

                setCalendarDataSet();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            lo.e("service disconnected...");

            service = null;
        }
    };

    private BroadcastReceiver rCalendarDataSet = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(LCBroadcast.CALENDAR_DATA_IS_READY)) {
                setCalendarDataSet();
            }
        }
    };

    private void setCalendarDataSet() {
        if (calendarDataSet != null) return;

        if (service == null) {
            bindServiceForCalendarData();
        } else {
            if (service.isPrepared()) {
                calendarDataSet = this.service.getWeeksDataSet();

                while (pendingActionListeners.size() > 0) {
                    IPendingActionListener _listener = pendingActionListeners.remove(0);
                    if (_listener != null) _listener.doAction();
                }
            } else {
                this.service.prepare();
            }
        }
    }

}
