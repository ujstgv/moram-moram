package com.lechat.lifestreaming.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.lechat.lifestreaming.fragments.MonthViewFragment;
import com.lechat.lifestreaming.fragments.TestFragment;
import com.lechat.lifestreaming.interfaces.IMonthNavigable;
import com.lechat.lifestreaming.utils.LCDateUtils;
import com.lechat.lifestreaming.utils.lo;

import java.util.Calendar;

/**
 * Created by lechat on 16. 4. 23..
 */
public class CalendarPagerAdapter extends FragmentStatePagerAdapter
        implements ViewPager.OnPageChangeListener {

    private ViewPager pager;
    private long todayInMillis;

    public CalendarPagerAdapter(FragmentManager fm, ViewPager pager, int initPage) {
        super(fm);
        this.pager = pager;

        init(initPage);
    }

    public void init(int initPage) {

        Calendar _today = LCDateUtils.getCalendar(true);
        LCDateUtils.clearHourUnit(_today);
        this.todayInMillis = _today.getTimeInMillis();

        pager.setAdapter(this);
        pager.addOnPageChangeListener(this);

        pager.setCurrentItem(initPage);
    }

    @Override
    public int getCount() {
        return Short.MAX_VALUE;
    }

    @Override
    public Fragment getItem(int position) {
        return MonthViewFragment.newInstance(position, todayInMillis);
    }

//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        lo.g("instantiateItem..." + position);
//        return super.instantiateItem(container, position);
//    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);

        lo.g("destroy Item..." + position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }

    /*
    ============================================================================================
    ViewPage update
    ============================================================================================
     */

//    @Override
//    public void startUpdate(ViewGroup container) {
//        super.startUpdate(container);
//
//    }
//
//    @Override
//    public void finishUpdate(ViewGroup container) {
//        super.finishUpdate(container);
//
//    }

    /*
    ============================================================================================
    Page event listener
    ============================================================================================
     */

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//        lo.v("page scrolled... " + position + "(" + positionOffset + ")");

    }

    @Override
    public void onPageSelected(int position) {
        lo.v("page selected... " + position);

        if(pager.getContext() instanceof IMonthNavigable) {
            Calendar _cal = LCDateUtils.getCalendar(false);
            _cal.setTimeInMillis(todayInMillis);
            _cal.add(Calendar.MONTH, LCDateUtils.getMonthOffsetForToday(position));

            ((IMonthNavigable)pager.getContext()).onMonthChanged(
                    _cal.get(Calendar.YEAR), _cal.get(Calendar.MONTH));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        lo.v("page scrollState changed... " + state);
    }
}
