package com.lechat.lifestreaming.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lechat.lifestreaming.R;
import com.lechat.lifestreaming.model.LCWeekOnCalendar;
import com.lechat.lifestreaming.utils.LCDateUtils;
import com.lechat.lifestreaming.utils.lo;

import java.util.Calendar;

/**
 * Created by lechat on 16. 4. 19..
 */

public class LCCalendarViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LCDateViewHolder[] arrDate;

        public LCDateViewHolder date0;
        public LCDateViewHolder date1;
        public LCDateViewHolder date2;
        public LCDateViewHolder date3;
        public LCDateViewHolder date4;
        public LCDateViewHolder date5;
        public LCDateViewHolder date6;

        public ViewHolder(View itemView) {
            super(itemView);

            date0 = new LCDateViewHolder(itemView.findViewById(R.id.date0));
            date1 = new LCDateViewHolder(itemView.findViewById(R.id.date1));
            date2 = new LCDateViewHolder(itemView.findViewById(R.id.date2));
            date3 = new LCDateViewHolder(itemView.findViewById(R.id.date3));
            date4 = new LCDateViewHolder(itemView.findViewById(R.id.date4));
            date5 = new LCDateViewHolder(itemView.findViewById(R.id.date5));
            date6 = new LCDateViewHolder(itemView.findViewById(R.id.date6));

            arrDate = new LCDateViewHolder[7];
            arrDate[0] = date0;
        }
    }

    public static class LCDateViewHolder {
        public TextView tvDate;
        public TextView tvAnniversary;
        public View icTrace;

        public LCDateViewHolder(View view) {
            tvDate = (TextView) view.findViewById(R.id.date);
            tvAnniversary = (TextView) view.findViewById(R.id.anniversary);
            icTrace = view.findViewById(R.id.ic_trace);
        }
    }

    private LCWeekOnCalendar[] mDataSet;
    private Calendar cal;
    private int currentMonth = -1;

    private int colorTextCurrentMonth;
    private int colorTextOtherMonth;

    public LCCalendarViewAdapter(@NonNull LCWeekOnCalendar[] dataSet) {
        mDataSet = dataSet;

        cal = Calendar.getInstance();
        cal.clear(Calendar.HOUR_OF_DAY);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
    }

    public LCWeekOnCalendar[] getDataSet() {
        return mDataSet;
    }

    public void setCurrentMonth(int month) {
        if (currentMonth == month) return;

        currentMonth = month;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();

        View v = LayoutInflater.from(context)
                .inflate(R.layout.element_calendar_week, parent, false);

        colorTextCurrentMonth = context.getResources().getColor(R.color.textBlack);
        colorTextOtherMonth = context.getResources().getColor(R.color.textLightGray);

        return new ViewHolder(v);
    }

    /**
     * 1. 리스트 위치에 따라 해당 주에 날짜를 설정
     * 2. 해당 날짜에 대한 데이터 적용
     *
     * @param holder   홀더~
     * @param position list 위치는 몇번째 주(week of year) 인지에 대한 참조
     */

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        cal.set(Calendar.WEEK_OF_YEAR, position + 2);
        LCDateUtils.clearHourUnit(cal);

        ViewHolder _holder = (ViewHolder) holder;

        setDateView(_holder.date0, cal);

        cal.add(Calendar.DATE, 1);
        setDateView(_holder.date1, cal);

        cal.add(Calendar.DATE, 1);
        setDateView(_holder.date2, cal);

        cal.add(Calendar.DATE, 1);
        setDateView(_holder.date3, cal);

        cal.add(Calendar.DATE, 1);
        setDateView(_holder.date4, cal);

        cal.add(Calendar.DATE, 1);
        setDateView(_holder.date5, cal);

        cal.add(Calendar.DATE, 1);
        setDateView(_holder.date6, cal);
    }

    private void setDateView(LCDateViewHolder dateView, Calendar calendar) {

        String _date = String.valueOf(calendar.get(Calendar.DATE));
        dateView.tvDate.setText(_date);

        boolean isFocusedMonth = calendar.get(Calendar.MONTH) == currentMonth;
        dateView.tvDate.setTextColor(isFocusedMonth ?
                colorTextCurrentMonth : colorTextOtherMonth);

//        String _anniversary = getStringTest(calendar);
//        dateView.tvAnniversary.setText(_anniversary);

        Calendar _today = LCDateUtils.getCalendar(true);
        LCDateUtils.clearHourUnit(_today);

        boolean isToday = _today.getTimeInMillis() == calendar.getTimeInMillis();
//                _today.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
//                _today.get(Calendar.DATE) == calendar.get(Calendar.DATE);
        dateView.icTrace.setVisibility(isToday ? View.VISIBLE : View.GONE);
    }

    private String getStringTest(Calendar calendar) {
        return calendar.get(Calendar.YEAR) + ", " + (calendar.get(Calendar.MONTH) + 1);
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }
}
