package com.lechat.lifestreaming;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.lechat.lifestreaming.adapter.CalendarPagerAdapter;
import com.lechat.lifestreaming.interfaces.IMonthNavigable;

/**
 * Created by lechat on 16. 4. 21..
 */
public class CalendarPagerActivity extends AppCompatActivity implements IMonthNavigable {

    private final static int startPage = Short.MAX_VALUE / 2;
    private CalendarPagerAdapter calendarAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_calendar_pager);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        calendarAdapter = new CalendarPagerAdapter(getSupportFragmentManager(), pager, startPage);

    }

    @Override
    public void onMonthChanged(int year, int month) {
        ActionBar _actionBar = getSupportActionBar();
        if(_actionBar != null) {
            _actionBar.setTitle(String.format("%d년 %d월", year, month + 1));
        }
    }
}
