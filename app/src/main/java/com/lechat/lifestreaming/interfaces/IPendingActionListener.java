package com.lechat.lifestreaming.interfaces;

public interface IPendingActionListener {
   void doAction();
}
