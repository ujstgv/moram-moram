package com.lechat.lifestreaming.interfaces;

/**
 * Created by lechat on 16. 4. 23..
 */
public interface IMonthNavigable {
    void onMonthChanged(int year, int month);
}
