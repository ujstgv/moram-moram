package com.lechat.lifestreaming.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by lechat on 16. 4. 18..
 */

public class LCDriveUploadService extends Service {

    public static final String START_UPLOAD = "start_upload";
    public static final String ABORT_UPLOAD = "abort_upload";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent.getExtras() == null) {
            return Service.START_NOT_STICKY;
        }

        String _action = intent.getStringExtra("actions");
        switch (_action) {
            case START_UPLOAD:

                break;

            case ABORT_UPLOAD:

                break;

            default:
                return Service.START_NOT_STICKY;
        }
        return Service.START_STICKY;
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
