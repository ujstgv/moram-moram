package com.lechat.lifestreaming.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.lechat.lifestreaming.global.LCBroadcast;
import com.lechat.lifestreaming.model.LCWeekOnCalendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by lechat on 16. 4. 19..
 */
public class CalendarDataService extends Service {

    /*
    ================================================================================================
    Local Binder
    ================================================================================================
     */

    private final IBinder binder = new LocalBinder();

    public class LocalBinder extends Binder {
        public CalendarDataService getService() {
            return CalendarDataService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /*
    ================================================================================================
    Prepare calendar data
    ================================================================================================
     */

    private boolean inProgress = false;
    private final static int START_YEAR = 1970;
    private final static int END_YEAR = 2170;
    private LCWeekOnCalendar[] weeksDataSet;

    public boolean isPrepared() {
        return !inProgress && weeksDataSet != null;
    }

    public void prepare() {
        task.run();
    }

    public LCWeekOnCalendar[] getWeeksDataSet() {
        return weeksDataSet;
    }

    private final Thread task = new Thread(new Runnable() {
        @Override
        public void run() {
            inProgress = true;
            initWeeksData();
            inProgress = false;

            if (weeksDataSet != null) {
                Intent _content = new Intent(LCBroadcast.CALENDAR_DATA_IS_READY);
                sendBroadcast(_content);
            }
        }
    });

    private void initWeeksData() {
        weeksDataSet = null;

        List<LCWeekOnCalendar> _numWeeksList = new ArrayList<>();
        Calendar _cal = Calendar.getInstance();
        int _cumulativeWeek = 0;
        int ordinalDay, weekDay, numberOfWeeks;

        for (int i = START_YEAR; i < END_YEAR; i++) {
            _cal.set(Calendar.YEAR, i);
            _cal.set(Calendar.MONTH, Calendar.DECEMBER);
            _cal.set(Calendar.DATE, 31);

            /**
             * http://stackoverflow.com/questions/8723862/calculate-number-of-weeks-in-a-given-year
             */
            ordinalDay = _cal.get(Calendar.DAY_OF_YEAR);
            weekDay = _cal.get(Calendar.DAY_OF_WEEK) - 1;
            numberOfWeeks = (ordinalDay - weekDay + 10) / 7;

            for (int j = _cumulativeWeek; j < numberOfWeeks + _cumulativeWeek; j++) {
                _numWeeksList.add(new LCWeekOnCalendar(j));
            }
            _cumulativeWeek += numberOfWeeks;
        }

        weeksDataSet = new LCWeekOnCalendar[_cumulativeWeek];
        _numWeeksList.toArray(weeksDataSet);
    }
}
