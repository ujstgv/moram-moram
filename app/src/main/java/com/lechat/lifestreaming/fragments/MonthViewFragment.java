package com.lechat.lifestreaming.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lechat.lifestreaming.R;
import com.lechat.lifestreaming.utils.LCDateUtils;
import com.lechat.lifestreaming.utils.lo;

import java.util.Calendar;

/**
 * Created by lechat on 16. 4. 22..
 */
public class MonthViewFragment extends Fragment {

    public static final String MONTH_INDEX_OFFSET = "month_index_offset";
    public static final String TODAY_IN_MILLIS = "today_in_millis";

    public static MonthViewFragment newInstance(int monthIndexOffset, long todayInMillis) {
        MonthViewFragment fragment = new MonthViewFragment();
        Bundle args = new Bundle();
        args.putInt(MONTH_INDEX_OFFSET, monthIndexOffset);
        args.putLong(TODAY_IN_MILLIS, todayInMillis);
        fragment.setArguments(args);
        return fragment;
    }


    private long todayInMillis;
    private int monthIndexOffset; // index of Short.MAX_VALUE(=num page)
    private int currentMonth;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        todayInMillis = getArguments().getLong(TODAY_IN_MILLIS);
        monthIndexOffset = LCDateUtils.getMonthOffsetForToday(getArguments().getInt(MONTH_INDEX_OFFSET));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View _view = inflater.inflate(R.layout.page_calendar_month, container, false);
        lo.w(this, "onCreateView > arguments...", getArguments());

        Calendar _cal = LCDateUtils.getCalendar(false);
        _cal.setTimeInMillis(todayInMillis);
        _cal.add(Calendar.MONTH, monthIndexOffset);

        _cal.set(Calendar.DATE, 1);
        int _curYear = _cal.get(Calendar.YEAR);
        currentMonth = _cal.get(Calendar.MONTH);
        int _firstDayOfWeek = _cal.get(Calendar.DAY_OF_WEEK);
        _cal.add(Calendar.DATE, (_firstDayOfWeek * -1) + 1);

        for (int weekIndex = 0; weekIndex < 6; weekIndex++) {
            LinearLayout _weekLayout =
                    (LinearLayout) _view.findViewById(getIdFromResources("week" + weekIndex));
            if (_cal.get(Calendar.YEAR) > _curYear ||
                    (_cal.get(Calendar.YEAR) == _curYear && _cal.get(Calendar.MONTH) > currentMonth)) {
                _weekLayout.setVisibility(View.GONE);
            } else {
                _weekLayout.setVisibility(View.VISIBLE);
                setWeek(_weekLayout, _cal);
            }
        }

        return _view;
    }

    private void setWeek(View weekLayout, Calendar calendar) {

        RelativeLayout _dayLayout;
        TextView _tvDate, _tvAnniversary;
        View _indicator;

        int _day;
        for (int i = 1; i < 8; i++) {

            _dayLayout = (RelativeLayout) weekLayout.findViewById(getIdFromResources("day" + i));
            _dayLayout.setVisibility(
                    calendar.get(Calendar.MONTH) == currentMonth ?
                            View.VISIBLE : View.INVISIBLE);
            _tvDate = (TextView) _dayLayout.findViewById(R.id.date);
            _tvAnniversary = (TextView) _dayLayout.findViewById(R.id.anniversary);

            _day = calendar.get(Calendar.DATE);
            _tvDate.setText(String.valueOf(_day));

            calendar.add(Calendar.DATE, 1);
        }

    }

    private int getIdFromResources(String name) {
        return getResources().getIdentifier(name, "id", getActivity().getPackageName());
    }

}
