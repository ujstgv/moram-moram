package com.lechat.lifestreaming.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lechat.lifestreaming.R;
import com.lechat.lifestreaming.utils.lo;

/**
 * Created by lechat on 16. 4. 22..
 */
public class TestFragment extends Fragment {

    public static final String MONTH_INDEX_OFFSET = "month_index_offset";
    public static final String TODAY_IN_MILLIS = "today_in_millis";

    public static TestFragment newInstance(int monthIndexOffset, long todayInMillis) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putInt(MONTH_INDEX_OFFSET, monthIndexOffset);
        args.putLong(TODAY_IN_MILLIS, todayInMillis);
        fragment.setArguments(args);
        return fragment;
    }

    public TestFragment() {
        lo.g(this, "construct");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        lo.w(this, "on attach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lo.w(this, "on create");
    }

    @Override
    public void onResume() {
        super.onResume();

        lo.w(this, "on resume");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {

        lo.w(this, "on create view");

        View v = inflater.inflate(R.layout.page_test, container, false);
        TextView tv = (TextView) v.findViewById(R.id.text);
        tv.setText("page: " + getArguments().getInt(MONTH_INDEX_OFFSET));
        return v;
    }

    @Nullable
    @Override
    public View getView() {

        lo.w(this, "get view");

        return super.getView();
    }
}
