package com.lechat.lifestreaming.model;

import java.util.Calendar;

/**
 * Created by lechat on 16. 4. 19..
 * <p/>
 * 1970년 2주차부터 시작되는 것이 전제
 * (1970년 1주차에는 1969년 날짜가 들어있어서 그냥 2주차부터로 했다능..)
 *
 * 클래스의 핵심 값으로 지정한 weekIndex 는 리스트의 위치이기도 하다.
 */
public class LCWeekOnCalendar {

    // 날짜 관련 정보
    public int weekIndex;
    public int year;
    public int firstDate;
    public int month1;
    public int month2;
    public boolean isFirstWeekOfMonth;

    private long boundaryStart;
    private long boundaryEnd;

    public LCWeekOnCalendar(int weekIndex) {

        this.weekIndex = weekIndex;

        Calendar _cal = Calendar.getInstance();
        _cal.clear(Calendar.HOUR_OF_DAY);
        _cal.clear(Calendar.MINUTE);
        _cal.clear(Calendar.SECOND);
        _cal.clear(Calendar.MILLISECOND);

        _cal.set(Calendar.YEAR, 1970);
        _cal.set(Calendar.MONTH, 0);
        _cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        _cal.set(Calendar.WEEK_OF_YEAR, weekIndex + 2);

        this.year = _cal.get(Calendar.YEAR);
        this.firstDate = _cal.get(Calendar.DATE);
        this.month1 = _cal.get(Calendar.MONTH);
        this.boundaryStart = _cal.getTimeInMillis();

        _cal.add(Calendar.DATE, 6);
        this.month2 = _cal.get(Calendar.MONTH);
        this.boundaryEnd = _cal.getTimeInMillis();

        this.isFirstWeekOfMonth = month1 != month2;
    }

    /**
     * @param timestamp 비교하고 싶은 시각의 TimeStamp
     * @return 이전 날짜면 -1, 이후 날짜면 1, 포함되는 날짜면 0
     */
    public int getComparison(long timestamp) {
        if (timestamp < boundaryStart) return -1;
        else if (timestamp > boundaryEnd) return 1;
        else return 0;
    }

    @Override
    public String toString() {
        return year + ", " + month2 + "(" + firstDate + "-" + (firstDate + 6) + ")";
    }

}
