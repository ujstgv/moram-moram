package com.lechat.lifestreaming.utils;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.lechat.lifestreaming.BuildConfig;

public class lo<T> {
	private static final String TAG = "lechat";

	public static <T> void v(T... messages)
	{
		if(BuildConfig.DEBUG)
			Log.v(TAG, convertMsgArgs(messages));
	}
	public static <T> void g(T... messages)
	{
		if(BuildConfig.DEBUG)
			Log.i(TAG, convertMsgArgs(messages));
	}
	public static <T> void d(T... messages)
	{
		if(BuildConfig.DEBUG)
			Log.d(TAG, convertMsgArgs(messages));
	}
	public static <T> void w(T... messages)
	{
		if(BuildConfig.DEBUG)
			Log.w(TAG, convertMsgArgs(messages));
		
	}
	public static <T> void e(T... messages)
	{
		if(BuildConfig.DEBUG)
			Log.e(TAG, convertMsgArgs(messages));
	}
	
	private static <T> String convertMsgArgs(T... messages)
	{
		String msg = "";
		String convertT;
//		int count = 0;
		for(T k : messages)
		{
			if(k instanceof Context || k instanceof Fragment) {
				convertT = "[" + k.getClass().getName() + "] ";
			} else {
				convertT = String.valueOf(k);
			}

			if(msg.length() > 0)
			{
				msg += "\n\t";
			}
			msg += convertT;
//			if(count%2 == 0){
//				msg += convertT;
//			}else{
//				if(msg.length() > 0)
//				{
//					msg += ", ";
//				}
//				msg += convertT + ": ";
//			}
//			count++;
		}
		return msg;
	}

}
