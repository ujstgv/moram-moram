package com.lechat.lifestreaming.utils;

import com.lechat.lifestreaming.model.LCWeekOnCalendar;

import java.util.Calendar;

/**
 * Created by lechat on 16. 4. 19..
 */
public class LCDateUtils {


    /**
     * 오늘에 해당하는 월의 첫주 Week Index 반환
     *
     * @return int WeekIndex
     */
    public static int getWeekIndexOfToday(LCWeekOnCalendar[] data) {

        Calendar _cal = getCalendar(true);
        clearHourUnit(_cal);

        long _todayTimeInMillis = _cal.getTimeInMillis();

        int _curIndex = data.length / 2;
        int _minIndexBound = 0;
        int _maxIndexBound = data.length;

        int _comparison;
        while ((_comparison = data[_curIndex].getComparison(_todayTimeInMillis)) != 0) {
            if (_comparison > 0) {
                _minIndexBound = _curIndex;
            } else {
                _maxIndexBound = _curIndex;
            }
            _curIndex = _minIndexBound + ((_maxIndexBound - _minIndexBound) / 2);
        }

        while(!data[_curIndex].isFirstWeekOfMonth) {
            _curIndex --;
        }

        return _curIndex;
    }

    public static int getMonthOffsetForToday(int monthIndex) {
        return monthIndex - (Short.MAX_VALUE / 2);
    }

    public static int getNumDayOfMonth(Calendar calendar) {
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    private static Calendar calendarInternal;
    public static Calendar getCalendar(boolean setToNow) {
        if(calendarInternal == null) {
            calendarInternal = Calendar.getInstance();
        }
        if(setToNow) {
            calendarInternal.setTimeInMillis(System.currentTimeMillis());
        }
        return calendarInternal;
    }
    public static void clearHourUnit(Calendar calendar) {
        calendar.clear(Calendar.HOUR_OF_DAY);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
    }
}
